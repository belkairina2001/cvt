#include <iostream>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <suitesparse/umfpack.h>
#include <omp.h>
using namespace std;

double f(double x, double y) {
    return (-3) / (2 * pow(abs(x - y) + 0.00001, 0.5));
}

double u(double x, double y) {
    return pow(abs(x-y) + 0.00001, 1.5);
}

int main() {
    int n = 100;
    int N = n * n; // N points
    int nnz = n * (n + 2 * (n - 1)) + 2 * (n - 1) * n;

    int* Ap;
    int* Ai;
    double* Ax;


    // Init Ap
    Ap = new int[N + 1];
    Ap[0] = 0;
    Ap[1] = 3;
    for (int i = 2; i < n; i++) {
        Ap[i] = Ap[i - 1] + 4;
    }
    Ap[n] = Ap[n - 1] + 3;

    for (int I = n; I < n * (n - 1); I += n) {
        Ap[I + 1] = Ap[I] + 4;
        for (int i = 2; i < n ; i++) {
            Ap[I + i] = Ap[I + i - 1] + 5;
        }
        Ap[I + n] = Ap[I + n - 1] + 4;
    }

    Ap[n * (n - 1) + 1] = Ap[n * (n - 1)] + 3;
    for (int i = 2; i < n; i++) {
        Ap[n * (n - 1) + i] = Ap[n * (n - 1) + i - 1] + 4;
    }
    Ap[n * n] = Ap[n * n - 1] + 3;

    //assert (nnz == Ap[N]);

    Ai = new int[nnz];
    Ax = new double[nnz];
    int j = 0; // j = 0, ..., nnz
    Ai[j] = 0;
    Ai[j + 1] = 1;
    Ai[j + 2] = n;

    Ax[j] = 4;
    Ax[j + 1] = -1;
    Ax[j + 2] = -1;
    j += 3;
    for (int i = 1; i < n - 1; i++) {
        Ai[j] = i - 1;
        Ai[j + 1] = i;
        Ai[j + 2] = i + 1;
        Ai[j + 3] = i + n;

        Ax[j] = -1;
        Ax[j + 1] = 4;
        Ax[j + 2] = -1;
        Ax[j + 3] = -1;
        j += 4;
    }

    Ai[j] = n - 2;
    Ai[j + 1] = n - 1;
    Ai[j + 2] = n - 1 + n;

    Ax[j] = -1;
    Ax[j + 1] = 4;
    Ax[j + 2] = -1;
    j += 3;


    for (int I = n; I < n * (n - 1); I += n) {
        Ai[j] = I - n;
        Ai[j + 1] = I;
        Ai[j + 2] = I + 1;
        Ai[j + 3] = I + n;

        Ax[j] = -1;
        Ax[j + 1] = 4;
        Ax[j + 2] = -1;
        Ax[j + 3] = -1;
        j += 4;
        for (int i = 1; i < n - 1 ; i++) {
            Ai[j] = I + i - n;
            Ai[j + 1] = I + i - 1;
            Ai[j + 2] = I + i;
            Ai[j + 3] = I + i + 1;
            Ai[j + 4] = I + i + n;

            Ax[j] = -1;
            Ax[j + 1] = -1;
            Ax[j + 2] = 4;
            Ax[j + 3] = -1;
            Ax[j + 4] = -1;
            j += 5;
        }
        Ai[j] = I + (n - 1) - n;
        Ai[j + 1] = I + (n - 1) - 1;
        Ai[j + 2] = I + (n - 1);
        Ai[j + 3] = I + (n - 1) + n;

        Ax[j] = -1;
        Ax[j + 1] = -1;
        Ax[j + 2] = 4;
        Ax[j + 3] = -1;
        j += 4;
    }

    Ai[j] = n * (n - 1) - n;
    Ai[j + 1] = n * (n - 1);
    Ai[j + 2] = n * (n - 1) + 1;


    Ax[j] = -1;
    Ax[j + 1] = 4;
    Ax[j + 2] = -1;
    j += 3;

    for (int i = 1; i < n - 1; i++) {
        Ai[j] = n * (n - 1) + i - n;
        Ai[j + 1] = n * (n - 1) + i - 1;
        Ai[j + 2] = n * (n - 1) + i;
        Ai[j + 3] = n * (n - 1) + i + 1;

        Ax[j] = -1;
        Ax[j + 1] = -1;
        Ax[j + 2] = 4;
        Ax[j + 3] = -1;
        j += 4;
    }

    Ai[j] = n * (n - 1) + (n - 1) - n;
    Ai[j + 1] = n * (n - 1) + (n - 1) - 1;
    Ai[j + 2] = n * (n - 1) + (n - 1);

    Ax[j] = -1;
    Ax[j + 1] = -1;
    Ax[j + 2] = 4;
    j += 3;

    for (int i = 0; i <= N; i++)
    {
        Ap[i] += 1;
    }
    for (int i = 0; i < nnz; i++)
    {
        Ai[i] += 1;
    }


    double* rgh = new double[N];
    double* x_real = new double[N];
    double* x = new double[N];

    // Init rgh, x_real;
    for (int i = 1; i < n + 1; i++) {
        double x_i = 1.0 * i / (n + 1);
        for (int j = 1; j < n + 1; j++) {
            double y_j = 1.0 * j / (n + 1);
            rgh[(i - 1 )* n + (j - 1)] = f(x_i, y_j) / ((n + 1) * (n + 1));
            rgh[(i - 1) * n + (j - 1)] += ((j == 1) * u(x_i, 0) + (j == n) * u(x_i, 1));
            rgh[(i - 1) * n + (j - 1)] += ((i == 1) * u(0, y_j) + (i == n) * u(1, y_j));

            x_real[(i - 1) * n + (j - 1)] = u(x_i, y_j);
        }
    }


    double Inform[UMFPACK_INFO], Inform2[UMFPACK_INFO];
    double *null = (double *) NULL ;
    void *Symbolic, *Numeric ;
    (void) umfpack_di_symbolic (N, N, Ap, Ai, Ax, &Symbolic, null, null) ;
    (void) umfpack_di_numeric (Ap, Ai, Ax, Symbolic, &Numeric, null, Inform) ;
    umfpack_di_free_symbolic (&Symbolic) ;
    (void) umfpack_di_solve (UMFPACK_Aat, Ap, Ai, Ax, x, rgh, Numeric, null, Inform2) ;
    umfpack_di_free_numeric (&Numeric) ;
    printf("Solve time -  %f\n",Inform2[UMFPACK_SOLVE_TIME] + Inform[UMFPACK_NUMERIC_TIME]);

    // Error norm
    double c_norm_err = 0;
    double x_real_c_norm = 0;
    for (int i = 0; i < N; i++) {
        if (abs(x_real[i]) > x_real_c_norm) {
            x_real_c_norm = abs(x_real[i]);
        }

        double t = abs (x_real[i] - x[i]);
        if (t > c_norm_err) {
            c_norm_err = t;
        }
    }
    cout<<c_norm_err<<endl;
    cout<<x_real_c_norm<<endl;
    cout << "Err / rgh_norm = " <<  c_norm_err / x_real_c_norm << endl;



    delete [] Ap;
    delete [] Ai;
    delete [] Ax;
    delete [] x;
    delete [] rgh;
}


