! ======================================================================
      Program  main
! ======================================================================
      implicit none
! ... user defined procedures 
      external  RefineRule,  CoarseRule 

      integer i, jlu, ipar, nnz, lfil, levs, jw, ju, iwk, labelV,bnd, labelB, &
              tri, labelT, iw,iDATAFEM, controlFEM, Dbc, dDATAFEM, &
              IA, JA, nRow, nCol, control, fixedb, fixedt,fixedv, labelc
              
              
      real*8 b, alu, sol, w, fpar, norma, vrt, rw, a,crv, rhs
      real*8 :: t1, t2, t3, t4, t5, t6
! ... nvmax - maximum number of mesh nodes
! ... ntmax - maximum number of mesh triangles
! ... nbmax - maximum number of boundary edges
      Integer nvmax, ntmax, nbmax, niamax, namax
      Parameter(nvmax = 1500000, ntmax = 2*nvmax, nbmax = 10000000)

! ... niamax - maximum number of matrix rows
! ... namax  - maximum number of non-zero matrix entries
      parameter(niamax = nvmax*4 + nvmax*4 + nvmax)
      !parameter(namax = 30*niamax )
      parameter(namax = 100000000)

! ... work memory
      Integer   MaxWr, MaxWi
      Parameter(MaxWr = 100000000, MaxWi = 500000000)

      !Integer  iW(MaxWi)
      !Real*8   rW(MaxWr)

! ... standard mesh arrays (see doc/user_guide.pdf for more detail)
! ... number of points, triangles and boundary edges
      Integer  nv, nt, nb, n

! ... coordinates of mesh points 
      !Double precision   vrt(2,nvmax)

! ... connectivity table for triangles and triangle labels
      !Integer  tri(3,ntmax), labelT(ntmax)

! ... connectivity table for boundary edges, and edge labels
      !Integer  bnd(2,nbmax), labelB(nbmax)
! ... maxlevel - maximum number of levels for refinement and coarsening
      Integer maxlevel
      Parameter(maxlevel = 150)
      integer iv1, iv2, iv3, label
      real*8 Uloc(3), trierr

! ... work memory (at least 11*ntmax+7)
      !Integer    MaxWi
      !Parameter(MaxWi = 5500000)

! ... history of bisection
      Logical history(maxlevel*ntmax)

! ... number of levels of refinement/coarsening
      integer  nlevel
      
! ... local variables
      Integer  ilevel, iERR, ipBCG
      Real*8 resinit
      Integer   imatvec(1), iprevec(1)
! ILU data
      Real*8    tau1,tau2,partlur,partlurout
      Integer   verb, UsedWr, UsedWi
! BiCGStab data
      External  matvec, prevec2
      Integer   ITER, INFO, NUNIT
      Real*8    RESID
! External routines from the BLAS library
      Real*8    ddot
      External  ddot, dcopy  
! ======================================================================
! For library aniFEM
! ======================================================================
      include 'fem2Dtri.fd'
      include 'assemble.fd'

      !Integer  IA(niamax), JA(namax), nRow, nCol
      !Real*8    A(namax), rhs!, RHS(niamax)

      !Integer  iDATAFEM(1), iSYS(MAXiSYS), controlFEM(3), Dbc
      !Real*8   dDATAFEM(1)

      EXTERNAL FEM2Dext, Dbc
  
      Logical  ifXbc
      Integer   ANI_Dnull, Dexact
      EXTERNAL  ANI_Dnull, Dexact


    allocatable labelV(:), fixedV(:), vrt(:,:), bnd(:,:), labelB(:), fixedB(:), &
              labelC(:), crv(:,:), tri(:,:), labelT(:), fixedT(:), control(:), &
              rW(:), iW(:), controlFEM(:), iDATAFEM(:), dDATAFEM(:), ia(:), ja(:), &
              a(:), b(:), alu(:), jlu(:), ju(:), w(:), levs(:), jw(:), sol(:), &
              fpar(:), ipar(:), rhs(:)
    allocate(labelV(nvmax), fixedV(nvmax), vrt(2, nvmax), bnd(2, nbmax), labelB(nbmax), &
           fixedB(nbmax), labelC(nbmax), crv(2, nbmax), tri(3, ntmax), labelT(ntmax), &
           fixedT(ntmax), control(6), rW(MaxWr), iW(MaxWi), iDATAFEM(1), controlFEM(3), &
           dDATAFEM(1), ia(nvmax), ja(namax), a(namax), b(nvmax), ipar(16), fpar(16))
    norma = 0
! ======================================================================
! Step 1: generate initial mesh
! ... Define a simple mesh for [0,1]^2 consisting of 2 triangles
      nv = 4
      vrt(1,1) = 0d0
      vrt(2,1) = 0d0
      vrt(1,2) = 0d0
      vrt(2,2) = 1d0
      vrt(1,3) = 1d0
      vrt(2,3) = 1d0
      vrt(1,4) = 1d0
      vrt(2,4) = 0d0

      nt = 2
      tri(1,1) = 1
      tri(2,1) = 2
      tri(3,1) = 3
      labelT(1) = 1 ! x-y<0 - material 1

      tri(1,2) = 3
      tri(2,2) = 4
      tri(3,2) = 1
      labelT(2) = 2 ! x-y>0 - material 2

      nb = 4
      bnd(1,1) = 1
      bnd(2,1) = 2
      labelB(1) = 1    ! Dirichlet face - label 1

      bnd(1,2) = 2
      bnd(2,2) = 3
      labelB(2) = 1    ! Dirichlet face - label 1

      bnd(1,3) = 3
      bnd(2,3) = 4
      labelB(3) = 2    ! Neumann face - label 2

      bnd(1,4) = 4
      bnd(2,4) = 1
      labelB(4) = 1    ! Dirichlet face - label 1

      Write(*,'(A,2I7)') &
        'Initial mesh:   numbers of nodes and triangles:',nv, nt

! ... draw mesh; demo has been activated
!     call graph(nv,vrt,nt,tri,'mesh_initial.ps')
      call graph_demo(nv,vrt, nt,tri, 'mesh_initial.ps','Initial mesh')
      
! Step 2: initialize data structure (work memory is at least 11*ntmax+7)

      iERR = 0
      call InitializeRCB(nt, ntmax, vrt, tri, MaxWi, iW, iERR)

      If(iERR.GT.0) Call errMesRCB(iERR, 'main', &
                             'error in InitializeRCB')


! Step 3: refine initial mesh nlevel times by local bisection. 
!         The rule is defined in RefineRule

      nlevel = 8
      call cpu_time(t1)
      Do ilevel = 1, nlevel
         Call LocalRefine( &
        nv, nvmax, nb, nbmax, nt, ntmax, &
        vrt, tri, bnd, labelB, labelT, &
        RefineRule, ilevel, maxlevel, history, &
        MaxWi, iW, iERR)

         If(iERR.GT.0) Call errMesRCB(iERR, 'main', &
                      'Error in LocalRefine')
      End do
! 
      Write(*,'(A,2I7)') &
        'Refined mesh:   numbers of nodes and triangles:',nv, nt

! ... draw mesh; demo graphics has been activated
!     call graph(nv,vrt,nt,tri,'mesh_final.ps')
      call graph_demo(nv,vrt, nt,tri, 'mesh_final.ps', &
               'Locally refined mesh')
      call cpu_time(t2)
      t2=t2-t1
      print*, 'Mesh time:', t2, 'sec.'


! ANIFEM
    call cpu_time(t3)
    dDATAFEM(1) = 0D0
    iDATAFEM(1) = 0

    do i = 1, nv
        if(vrt(1,i) == 0d0) then
        labelV(i) = 1
        else if(vrt(2,i) == 1d0) then
        labelV(i) = 2
        else if(vrt(1,i) == 1d0) then
        labelV(i) = 3
        else if(vrt(2,i) == 0d0) then
        labelV(i) = 4
        else
        labelV(i) = 0
        end if
    end do

    controlFEM(1) = IOR(MATRIX_GENERAL, FORMAT_CSR)
    controlFEM(2) = 1

    call BilinearFormTemplate(nv, nb, nt, vrt, labelV, bnd, labelB, tri, labelT, &
                            FEM2Dext, dDATAFEM, iDATAFEM, controlFEM, &
                            nvmax, namax, ia, ja, a, b, nRow, nCol, &
                            MaxWi, MaxWr, iW, rW)

    call cpu_time(t4)
    t4=t4-t3
    print*, 'Time of mesh system:', t4, 'sec.'
    print*

    iERR = 0
    nnz = ia(nRow + 1) - ia(1)
    iwk = 15 * nnz + nRow
    allocate(w(5 * nRow), jw(3 * nRow), levs(iwk), alu(iwk), jlu(iwk), ju(iwk), sol(nRow), rhs(nRow))
    do i = 1, nRow
        rhs(i) = ((vrt(1,i) - vrt(2,i))*(vrt(1,i) - vrt(2,i)) + 0.00001)**3.0
    end do

    do i = 1, nRow
        sol(i) = 0d0
    end do
  
    call cpu_time(t5)
! precondition ilu2
! call iluk(nRow,a,ja,ia,lfil,alu,jlu,ju,levs,iwk,w,jw,iERR)
    verb = 0 ! verbose no
    tau1 = 1d-2
    tau2 = 1d-3
    partlur = 0.5
    ierr = 0

    Call iluoo(nRow, ia, ja, a, tau1, tau2, verb, &
               rW, iW, MaxWr, MaxWi, partlur, partlurout,&
               UsedWr, UsedWi, ierr)

     If(ierr.ne.0) Then
         Write(*,'(A,I7)') 'Initialization of iluoo failed, ierr=', ierr
         Stop 911
      End if
      Write(*,'(A)') ' Recommended memory ILU2 + BiCGs:'
      Write(*,'(A,F12.2)') ' partlur =', partlurout
      Write(*,'(A,I12)') ' MaxWr   =', UsedWr+8*nRow
      Write(*,'(A,I12)') ' MaxWi   =', UsedWi

      If(UsedWr+8*nRow.gt.MaxWr) Then
         Write(*,'(A,I7)') 'Increase MaxWr to ', UsedWr+8*nRow
         Stop 911
      End if
      ipBCG = UsedWr + 1

! init
    Call dcopy(nRow,0d0,0,sol,1)

!  compute initial residual norm
      resinit = ddot(nRow,b,1,b,1)
      resinit = dsqrt(resinit)
      If(resinit.eq.0d0) Then 
         Write(*,'(A)') 'rhs=0, nothing to solve!'
         Stop 911
      End if

! solve system 
! b = f

    ITER = 1000             ! max number of iterations
    RESID = 1d-10*resinit   ! threshold for \|RESID\|
    INFO  = 0               ! no troubles on imput
    NUNIT = 6               ! output channel
    iprevec(1) = nRow          ! single entry required: system size 
    imatvec(1) = nRow          ! single entry required: system size 

    call cpu_time(t1)

    call slpbcgs(prevec2, iprevec, iW,rW,&
         matvec,  imatvec, ia,ja,a,&
         rW(ipBCG), nRow, 8,&
         nRow, b, sol,&
         ITER, RESID,&
         INFO, NUNIT)


    If(INFO.ne.0) Then
      Write(*,'(A)') 'BiCGStab failed'
      Stop 911
    End if

    call cpu_time(t2)

    write(*,*) 'Time: ', t2 - t1
!!!!!!!!!!!!!!!!!!!!!!!! 
! === compute Lp-error
         norma = 0D0
         Do n = 1, nt
            label = labelT(n)

            Do i = 1, 3
               iv1 = tri(i, n)
               Uloc(i) = sol(iv1)
            End do

            iv1 = tri(1, n)
            iv2 = tri(2, n)
            iv3 = tri(3, n)
            Call fem2Derr(vrt(1,iv1), vrt(1,iv2), vrt(1,iv3), 2,&
                         IDEN, FEM_P1, Uloc, Dexact, dDATAFEM, iDATAFEM,&
                         label, ANI_Dnull, dDATAFEM,iDATAFEM,controlFEM, 5, trierr)

            !norma = max(norma, trierr)
            norma = norma + trierr**2
         End do


  print*, 'Solve error:', norma
  Stop 
  End


subroutine FEM2Dext(XY1, XY2, XY3, &
                    lbT, lbB, lbV, dDATA, iDATA, iSYS, &
                    LDA, A, F, nRow, nCol, &
                    templateR, templateC)
  implicit none

  include 'fem2Dtri.fd'
  include 'assemble.fd'

  real*8   XY1(*), XY2(*), XY3(*)
  integer  lbT, lbB(3), lbV(3)
  real*8   dDATA(*)
  integer  iDATA(*), iSYS(*), LDA
  real*8   A(LDA, *), F(*)
  integer  templateR(*), templateC(*)
  integer  Ddiff, Drhs, Dbc
  external Ddiff, Drhs, Dbc
  integer  i, j, k, ir, ic, label, ibc, nRow, nCol
  real*8   XYP(2, 3)
  real*8   x, y, eBC(1)
  logical  ifXbc
  real*8   hmin, hmin1, hmin2, hmin3
  COMMON   /cml/hmin

  nRow = 3
  nCol = 3

  do i = 1, 3
    templateR(i) = Vdof
    templateC(i) = Vdof
  end do

  label = lbT

  if((XY1(1) .eq. XY1(2)) .OR. (XY2(1) .eq. XY2(2)) .OR. (XY3(1) .eq. XY3(2))) then

    hmin1 = dsqrt((XY1(1) - XY2(1))**2 + (XY1(2) - XY2(2))**2)
    hmin2 = dsqrt((XY1(1) - XY3(1))**2 + (XY1(2) - XY3(2))**2)
    hmin3 = dsqrt((XY2(1) - XY3(1))**2 + (XY2(2) - XY3(2))**2)

    if((hmin1 <= hmin2) .AND. (hmin1 <= hmin3)) then
      hmin = hmin1
    else if((hmin2 <= hmin1) .AND. (hmin2 <= hmin3)) then
      hmin = hmin2
    else if((hmin3 <= hmin1) .AND. (hmin3 <= hmin2)) then
      hmin = hmin3
    end if
  end if

  call fem2Dtri(XY1, XY2, XY3, &
                GRAD, FEM_P1, GRAD, FEM_P1, &
                label, Ddiff, dDATA, iDATA, iSYS, 2, &
                LDA, A, ir, ic)

  call fem2Dtri(XY1, XY2, XY3, &
                IDEN, FEM_P0, IDEN, FEM_P1, &
                label, Drhs, dDATA, iDATA, iSYS, 4, &
                1, F, ir, ic)

  do i = 1, 2
    XYP(i, 1) = XY1(i)
    XYP(i, 2) = XY2(i)
    XYP(i, 3) = XY3(i)
  end do

  do k = 1, 3
    if(lbV(k) .NE. 0) then
      x = XYP(1, k)
      y = XYP(2, k)

      label = lbV(k)
      ibc = Dbc(x, y, label, dDATA, iDATA, iSYS, eBC)

      if(ifXbc(ibc, BC_DIRICHLET)) then
        call applyDIR(LDA, nRow, A, F, k, eBC(1))
      end if
    end if
  end do

  return
end

integer function Ddiff(x, y, label, dDATA, iDATA, iSYS, Coef)
  implicit none

  include 'fem2Dtri.fd'

  real*8  dDATA(*), x, y, Coef(MaxTensorSize, *)
  integer iDATA(*), label, iSYS(*)

  iSYS(1) = 1
  iSYS(2) = 1

  Coef(1, 1) = 1D0
  Coef(2, 2) = 1D0
  Coef(2, 1) = 0D0
  Coef(1, 2) = 0D0
  Ddiff = TENSOR_SCALAR

  return
end

integer function Drhs(x, y, label, dDATA, iDATA, iSYS, Coef)
  implicit none

  include 'fem2Dtri.fd'

  real*8  dDATA(*), x, y, Coef(MaxTensorSize, *), myrhs
  integer iDATA(*), label, iSYS(*)
  external myrhs
  iSYS(1) = 1
  iSYS(2) = 1


  Coef(1, 1) = myrhs(x, y)
  Drhs = TENSOR_SCALAR

  return
end

integer function Dbc(x, y, label, dDATA, iDATA, iSYS, Coef)
  implicit none

  include 'fem2Dtri.fd'

  real*8  dDATA(*), x, y, Coef(MaxTensorSize, *), myexact
  integer iDATA(*), label, iSYS(*)
  external myexact

  iSYS(1) = 1
  iSYS(2) = 1
  
   Dbc = BC_DIRICHLET
   Coef(1, 1) = myexact(x, y)

  return
end
integer function Dexact(x, y, label, dDATA, iDATA, iSYS, Coef)
  implicit none

  include 'fem2Dtri.fd'

  real*8  dDATA(*), x, y, Coef(MaxTensorSize, *), myexact
  integer iDATA(*), label, iSYS(*)
  external myexact

  iSYS(1) = 1
  iSYS(2) = 1
  
  Coef(1, 1) = myexact(x, y)
  Dexact = TENSOR_SCALAR

  return
end

function distdot(n,x,ix,y,iy)
  integer n, ix, iy
  real*8 distdot, x(*), y(*), ddot

  external ddot

  distdot = ddot(n,x,ix,y,iy)

  return
end


! ======================================================================
! User routine  defines the rule for local refinement depending on 
! current level.
! On input: nE  current number of elements
!           IPE current connectivity table
!           XYP current coordinates
!           ilevel current level of refinement
! On output: verf marker for refinement of each element
!            (0 - no need to refine, 1 - refine by single bisection,
!             2 - refine by two levels of bisection preserving the shape)
! ======================================================================
      Subroutine RefineRule (nE, IPE, XYP, verf, ilevel)
! ======================================================================
      implicit none
      
      Integer             nE
      Integer             IPE(3,*)
      Double precision    XYP(2,*)
      Integer             verf(*)
      Integer             ilevel

      Integer             i
      Double precision    xy, xy1, xy2, xy3

! ================================================================
      If (ilevel .le. 0) then
        Do i = 1, nE
          verf(i) =  2 ! two levels of bisection (keep the shape)
        End do
      Else  ! refine towards the diagonal y=x
        Do i = 1, nE
          xy1 = XYP(2,IPE(1,i)) - XYP(1,IPE(1,i))
          xy2 = XYP(2,IPE(2,i)) - XYP(1,IPE(2,i))
          xy3 = XYP(2,IPE(3,i)) - XYP(1,IPE(3,i))
          xy  = (xy1 **2 + xy2 **2) * &
          (xy1 **2 + xy3 **2) * &
          (xy2 **2 + xy3 **2)
       
          If (xy .eq. 0) then 
            verf(i) =  2 ! two levels of bisection (keep the shape)
          else
            verf(i) =  0 ! no need to refine
          End if 
       End do
       End if 

       Return
       End


       real*8 function myrhs(x, y)
           implicit none
           real*8 x, y
           myrhs = -4
           !myrhs = -3 / (2 * sqrt(abs(x-y) + 0.00001));
       end function myrhs

       real*8 function myexact(x, y)
           implicit none
           real*8 x, y
           myexact = x**2 + y**2
           !myexact = (abs(x-y) + 0.00001)**1.5
       end function myexact
