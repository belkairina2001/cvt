#include <stdio.h>
#include <suitesparse/umfpack.h>
#include <time.h>
#include <math.h>
int main(){
    int n;
    int *Ap; // (n + 1)-dimension array with, last element - nnz - dimension of Ai and Ax
    int *Ai; // nnz - dimension array column index
    double *Ax; // nnz - dimension array - non-zero values
    double *b;// right vector - n dimension
    double *x;
    double *null = (double *) NULL ;
    for (int i = 1; i < 5; ++i)
    {
        char fname[250];
        sprintf(fname, "%d.dat", i);
        FILE *file = fopen(fname, "r");
        fscanf(file, "%d", &n);
        printf("System %d\nN = %d\n", i, n);
        Ap = new int[n + 1];
        for (int j = 0; j < n + 1; ++j){
            fscanf(file, "%d", &Ap[j]);
            Ap[j]--;
        }
        int nnz = Ap[n];
        Ai = new int[nnz];
        for (int j = 0; j < nnz; ++j){
            fscanf(file, "%d", &Ai[j]);
            Ai[j]--;
        }
        Ax = new double[nnz];
        for (int j = 0; j < nnz; ++j)
            fscanf(file, "%lf", &Ax[j]);
        fclose(file);
        b = new double[n];
        for (int j = 0; j < n; ++j)
            b[j] = sin(j);
        x = new double[n];
        clock_t cur = clock();
        void *Symbolic, *Numeric ;
        (void) umfpack_di_symbolic (n, n, Ap, Ai, Ax, &Symbolic, null, null) ;
        (void) umfpack_di_numeric (Ap, Ai, Ax, Symbolic, &Numeric, null, null) ;
        printf("Init time: %f\n", (clock() - (double)cur ) / CLOCKS_PER_SEC / 6);
        cur = clock();
        umfpack_di_free_symbolic (&Symbolic) ;
        (void) umfpack_di_solve (UMFPACK_A, Ap, Ai, Ax, x, b, Numeric, null, null) ;
        umfpack_di_free_numeric (&Numeric) ;
        printf("Solve time: %f\n", (clock() - (double)cur) / CLOCKS_PER_SEC / 6);
        delete [] Ap;
        delete [] Ai;
        delete [] Ax;
        delete [] b;
        delete [] x;
    }
    return (0) ;
}
